
INSERT INTO Spatula VALUES 
	(DEFAULT, 'Tefal', 'Food', '45cm', 'Black', 25.00, 1000),
    (DEFAULT, 'USANA', 'Drugs', '75cm', 'Red', 199.99, 1000),
    (DEFAULT, 'Scotch', 'Drugs', '60cm','Black',155.00, 1000),
    (DEFAULT, 'Dulux', 'Paints', '25cm', 'Brown', 35.00, 1000),
    (DEFAULT, 'Winston','Plaster', '30cm', 'Silver', 29.00, 1000),
    (DEFAULT, 'Jacob', 'Drugs', '55cm', 'Blue', 244.00, 0),
    (DEFAULT, 'N2', 'Paints', '33cm', 'Wood Brown', 40.40, 0),
    (DEFAULT, 'Inspire', 'Food','50cm', 'Matte Black', 24.99, 0),
    (DEFAULT, 'Wimbledon', 'Plaster', '25cm', 'Dark Green', 21.00, 0),
    (DEFAULT, 'Neurofen', 'Drugs', '70cm', 'Jet Black',  134.00, 0);
    
INSERT INTO `Order` VALUES
	(DEFAULT, '2016-10-15 21:03:40', 'Nelson Chen', 'Delivery'),
    (DEFAULT, '2012-01-22 09:12:05', 'Nelson Chen', 'Pick Up'),
    (DEFAULT, '2015-03-18 12:14:56', 'Nelson Chen', 'Delivery'),
    (DEFAULT, '2003-05-13 23:03:42', 'Nelson Chen', 'Pick Up'),
    (DEFAULT, '2007-02-28 17:55:33', 'Nelson Chen', 'Pick Up');
    
INSERT INTO `OrderLineItem` VALUES (1,DEFAULT,1), (2,DEFAULT,2), (3,DEFAULT,3), (4,DEFAULT,4), (5,DEFAULT,5);    