<html>
<head>
  <link rel='stylesheet' type='text/css' href='style.css'>
  <title>Order History</title>
</head>
<body>

  <h1>Order History</h1>

  <a href="/nelsonc1/index.php">Home</a> -
  <a href="/nelsonc1/browse.php">Browse</a> -
  <a href="/nelsonc1/order.php">Order</a> -
  <a href="/nelsonc1/history.php">Order History</a>
  <br>
  <br>
  <?php
    $con = mysqli_connect("info20003db.eng.unimelb.edu.au","nelsonc1","nelsonc1","nelsonc1");

    // Check connection
    if (mysqli_connect_errno()) {
      echo "Could not connect to MySQL for the following reason: " . mysqli_connect_error();
    }

    // Fetch data from MySQL
    $history = mysqli_query($con, "SELECT * FROM `Order` NATURAL JOIN `OrderLineItem` ORDER BY idOrder");

    // Output Table
    echo "<table border='1' cellpadding='7'>";
    echo "<tr><td><b>Order ID</b></td>";
    echo "<td><b>Staff Member</b></td>";
    echo "<td><b>Spatula ID</b></td>";
    echo "<td><b>Quantity</b></td>";
    echo "<td><b>Time</b></td>";
    echo "<td><b>Customer Details</b></td>";

    // Output table data
    while($row = mysqli_fetch_array($history)){
      echo "<tr>";
      echo "<td>" . $row['idOrder'] . "</td>";
      echo "<td>" . $row['ResponsibleStaffMember'] . "</td>";
      echo "<td>" . $row['idSpatula'] . "</td>";
      echo "<td>" . $row['Quantity'] . "</td>";
      echo "<td>" . $row['RequestedTime'] . "</td>";
      echo "<td>" . $row['CustomerDetails'] . "</td>";
      echo "</tr>";
    }

   ?>
</body>
</html>
