<html>
<head>
  <link rel='stylesheet' type='text/css' href='style.css'>
  <title>Order Page</title>
</head>
<body>

  <h1>Orders</h1>

  <a href="/nelsonc1/index.php">Home</a> -
  <a href="/nelsonc1/browse.php">Browse</a> -
  <a href="/nelsonc1/order.php">Order</a> -
  <a href="/nelsonc1/history.php">Order History</a>
  <br>
  <br>
  <form method='POST' action='submitOrder.php'>
    <b>Customer Details:</b><br/>
    <textarea cols='50' rows='10' name='details' value=''></textarea>
    <br>
    <br/>
    Responsible Staff Member: <input type='text' name='staff' value=''>
    <br>
    <br>
    <?php
      $con = mysqli_connect("info20003db.eng.unimelb.edu.au","nelsonc1","nelsonc1","nelsonc1");

      // Check connection
      if (mysqli_connect_errno()) {
      	echo "Could not connect to MySQL for the following reason: " . mysqli_connect_error();
      }

      // Create Table
      echo "<table border='1' cellpadding='5'>";
      echo "<tr><td><b>Spatula ID</b></td>";
      echo "<td><b>Name</b></td>";
      echo "<td><b>Type</b></td>";
      echo "<td><b>Size</b></td>";
      echo "<td><b>Colour</b></td>";
      echo "<td><b>Price</b></td>";
      echo "<td><b>Quantity In Stock</b></td>";
      echo "<td><b>Order Quantity</b></td></tr>";

      // Fetch data from MySQL
      $result = mysqli_query($con,"SELECT idSpatula, ProductName, Type, Size, Colour, Price, QuantityInStock FROM Spatula WHERE QuantityInStock <> 0 ORDER BY idSpatula");

      // Input rows and data
      while($row = mysqli_fetch_array($result)) {
      	echo "<tr>";
      	echo "<td>" . $row['idSpatula'] . "</td>";
        echo "<td>" . $row['ProductName'] . "</td>";
        echo "<td>" . $row['Type'] . "</td>";
        echo "<td>" . $row['Size'] . "</td>";
        echo "<td>" . $row['Colour'] . "</td>";
        echo "<td>" . $row['Price'] . "</td>";
        echo "<td>" . $row['QuantityInStock'] . "</td>";
        echo "<td><input type='number' name='count[]' value='0'></td>";
      	echo "</tr>";
      }

      echo "</table>";

      mysqli_close($con);
    ?>
    <br>
    <input type='submit' value='Submit'/>
  </form>

</body>
</html>
