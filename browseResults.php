<html>
<head>
  <link rel='stylesheet' type='text/css' href='style.css'>
  <title>Search</title>
</head>
<body>
  <h1>Search Results</h1>

  <a href="/nelsonc1/index.php">Home</a> -
  <a href="/nelsonc1/browse.php">Browse</a> -
  <a href="/nelsonc1/order.php">Order</a> -
  <a href="/nelsonc1/history.php">Order History</a>
  <br>
  <br>
  <?php
    $con = mysqli_connect("info20003db.eng.unimelb.edu.au","nelsonc1","nelsonc1","nelsonc1");

    // Check connection
    if (mysqli_connect_errno()) {
      echo "Could not connect to MySQL for the following reason: " . mysqli_connect_error();
    }

    // Fetch input data from browse.php
    $name = $_POST['spatula'];
    $type = $_POST['type'];
    $size = $_POST['size'];
    $colour = $_POST['colour'];
    $price = $_POST['price'];

    // Form MySQL query
    $query = "SELECT idSpatula, ProductName FROM `Spatula` ";

    if ($name != '' || $type != 'default' || $size != '' || $colour != '' || $iprice != ''){
      $query .= "WHERE ";
    }

    if ($name != ''){
      $query .= "`ProductName` LIKE '%$name%' ";
    }

    if ($type != 'default'){
      if ($name != ''){
        $query.= "AND ";
      }
      $query .= "`Type` = '$type' ";
    }

    if ($size != ''){
      if ($type != 'default' || $name != ''){
        $query.= "AND ";
      }
      $query .= "`Size` LIKE '%$size%' ";
    }

    if($colour != ''){
      if ($size != '' || $type != 'default' || $name != ''){
        $query.= "AND ";
      }
      $query .= "`Colour` LIKE '%$colour%' ";
    }

    if($price != ''){
      if ($size != '' || $type != 'default' || $name != '' || $colour != ''){
        $query.= "AND ";
      }
      $query .= "`Price` = '$price' ";
    }

    $query .= "ORDER BY `ProductName`;";

    $result = mysqli_query($con, $query);

    // Output search results as URLs
    while($row = mysqli_fetch_array($result)) {
      echo "<a href='results.php?sID=";
      echo $row['idSpatula'];
      echo "'>".$row['ProductName']."</a><br><br>";
    }

    echo "<br><br>";

  ?>
  <button type='button' onclick="location.href='browse.php'">Back</button>
</body>
</html>
