<html>
<head>
  <link rel='stylesheet' type='text/css' href='style.css'>
  <title>Spatula Details</title>
</head>
<body>
  <h1>Spatula Details</h1>

  <a href="/nelsonc1/index.php">Home</a> -
  <a href="/nelsonc1/browse.php">Browse</a> -
  <a href="/nelsonc1/order.php">Order</a> -
  <a href="/nelsonc1/history.php">Order History</a>
  <br>
  <br>
  <form>
  <?php

    $con = mysqli_connect("info20003db.eng.unimelb.edu.au","nelsonc1","nelsonc1","nelsonc1");

    // Check connection
    if (mysqli_connect_errno()) {
      echo "Could not connect to MySQL for the following reason: " . mysqli_connect_error();
    }

    // MySQL query
    $query = "SELECT * FROM Spatula WHERE idSpatula=".$_GET[sID];

    // Fetch data from MySQL
    $result = mysqli_query($con, $query);

    // Output spatula details
    while ($row = mysqli_fetch_array($result)){

      echo "<strong>Spatula ID: </strong>".$row['idSpatula']."<br><br>";
      echo "<strong>ProductName: </strong>".$row['ProductName']."<br><br>";
      echo "<strong>Type: </strong>".$row['Type']."<br><br>";
      echo "<strong>Size: </strong>".$row['Size']."<br><br>";
      echo "<strong>Colour: </strong>".$row['Colour']."<br><br>";
      echo "<strong>Price: </strong>".$row['Price']."<br><br>";
      echo "<strong>Quantity In Stock: </strong>".$row['QuantityInStock']."<br><br>";
    }

  ?>

  <button type='button' onclick="location.href='browse.php'">Back</button>
</form>
</body>
</html>
