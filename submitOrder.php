<html>
<head>
  <link rel='stylesheet' type='text/css' href='style.css'>
  <title>Order Details</title>
</head>
<body>

  <h1>Order Status</h1>

  <a href="/nelsonc1/index.php">Home</a> -
  <a href="/nelsonc1/browse.php">Browse</a> -
  <a href="/nelsonc1/order.php">Order</a> -
  <a href="/nelsonc1/history.php">Order History</a>
  <br>
  <br>
  <form method="POST" action="order.php">
  <?php
    $con = mysqli_connect("info20003db.eng.unimelb.edu.au","nelsonc1","nelsonc1","nelsonc1");

    // Check connection
    if (mysqli_connect_errno()) {
      echo "Could not connect to MySQL for the following reason: " . mysqli_connect_error();
    }

    // Check for customer details
    $customer_details = $_POST['details'];
    if ($customer_details == '') {
      echo "ERROR: Please enter customer details";
      echo "<button id='return_button'>Return to order</button>";
      session_abort();
    }

    // Check for staff name
    $staffname = $_POST['staff'];
    if ($staffname == '') {
      echo "ERROR: Please enter a staff name!<br><br>";
      echo "<button id='return_button'>Return to order</button>";
      session_abort();
    } else {
      echo "<strong>Reponsible Staff Member: " . $staffname . "</strong><br><br>";
    }


    mysqli_autocommit($con, TRUE);
    
    $id = 0;
    $order_count = 0;
    foreach ($_POST[count] as $value){
      $id++;

      //Check for invalid numbers. Otherwise, input data to MySQL
      if (! ctype_digit(strval($value))){
        echo "Invalid value. Please resubmit order.<br>";
        session_abort();
      } elseif ($value > 0){
        $order_count += $value;
        $time = strftime("%Y-%m-%d %H:%M:%S");
        mysqli_query($con, "INSERT INTO `Order` (`idOrder`,`RequestedTime`,`ResponsibleStaffMember`,`CustomerDetails`) VALUES (DEFAULT, '$time', '$staffname', '$customer_details')");
        mysqli_query($con, "INSERT INTO `OrderLineItem` (`idSpatula`,`idOrder`,`Quantity`) VALUES ('$id', DEFAULT, '$value')");
        mysqli_query($con, "UPDATE `Spatula` SET `QuantityInStock` = `QuantityInStock` - $value WHERE `idSpatula` = $id");
      }
    }

    // Validate successful data entry
    if ($order_count > 0){
      echo "<p>Your order has been succesfully placed!</p>";
      echo "<button id='order_more'>Order more!</button>";
    } else {
      echo "<p>You have not placed any order!</p>";
      echo "<button id='return_button'>Return to order</button>";
    }

    echo "<br>";
  ?>
  </form>
</body>
</html>
