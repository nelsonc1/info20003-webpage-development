<html>
<head><title>Lab F</title></head>
<body>
<h1> Lab F - SELECT Example</h1>
<p>
This PHP code runs an SQL query, and displays the answers in an HTML table.
</p>
<p>
<br>



<?php

//replace the following with your details. Dbname is your username by default.
$con = mysqli_connect("info20003db.eng.unimelb.edu.au","username","passwd","dbname");

// Check connection
if (mysqli_connect_errno()) {
	echo "Could not connect to MySQL for the following reason: " . mysqli_connect_error();
}


/* this lists the name and release date of all action movies */ 
$result = mysqli_query($con,"SELECT name, release_date FROM Movie WHERE genre = 'action'");

while($row = mysqli_fetch_array($result)) {
	echo $row['name'] . " " . $row['release_date'];
}
mysqli_close($con);
?>

</body>
</html>

